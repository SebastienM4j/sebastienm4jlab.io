import { createApp } from 'vue'
import AppCvPapierIntegral from './AppCvPapierIntegral.vue'
import store from './store'
import naive, { NThemeEditor } from 'naive-ui'

createApp(AppCvPapierIntegral)
    .use(store)
    .use(naive)
    //.component('NThemeEditor', NThemeEditor)
    .mount('#app')
