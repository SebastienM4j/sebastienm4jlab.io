import { createApp } from 'vue'
import AppCvPapierSimplified from './AppCvPapierSimplified.vue'
import store from './store'
import naive, { NThemeEditor } from 'naive-ui'

createApp(AppCvPapierSimplified)
    .use(store)
    .use(naive)
    //.component('NThemeEditor', NThemeEditor)
    .mount('#app')
