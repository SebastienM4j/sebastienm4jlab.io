import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import naive, { NThemeEditor } from 'naive-ui'

createApp(App)
    .use(router)
    .use(store)
    .use(naive)
    //.component('NThemeEditor', NThemeEditor)
    .mount('#app')
