import { createStore } from 'vuex'
import { darkTheme } from 'naive-ui'

function month(month) {
    return month-1;
}

const cv = {
    debutCarriere: new Date(2006, month(5), 15),

    competencesTechniques: [
        { type: "langage", categorie: "dev", nom: "Java", niveau: 95, utilisation: "quotidienne" },
        { type: "langage", categorie: "dev", nom: "Kotlin", niveau: 15, utilisation: "à titre personnel" },
        { type: "langage", categorie: "dev", nom: "C#", niveau: 15, utilisation: "ponctuelle" },
        { type: "langage", categorie: "dev", nom: "Groovy", niveau: 10, utilisation: "ponctuelle" },
        { type: "langage", categorie: "dev", nom: "Go", niveau: 10, utilisation: "à titre personnel" },
        { type: "langage", categorie: "web", nom: "Javascript", niveau: 40, utilisation: "ponctuelle" },
        { type: "langage", categorie: "web", nom: "Typescript", niveau: 30, utilisation: "ponctuelle" },
        { type: "langage", categorie: "web", nom: "HTML", niveau: 80, utilisation: "ponctuelle" },
        { type: "langage", categorie: "web", nom: "CSS", niveau: 30, utilisation: "ponctuelle" },
        { type: "langage", categorie: "bdd", nom: "SQL", niveau: 75, utilisation: "régulière" },
        { type: "langage", categorie: "bdd", nom: "PL/SQL", niveau: 20, utilisation: "par le passé" },
        { type: "langage", categorie: "bdd", nom: "JPQL", niveau: 80, utilisation: "régulière" },
        { type: "langage", categorie: "format", nom: "XML", niveau: 75, utilisation: "ponctuelle" },
        { type: "langage", categorie: "format", nom: "JSON", niveau: 90, utilisation: "quotidienne" },
        { type: "langage", categorie: "format", nom: "YAML", niveau: 90, utilisation: "quotidienne" },
        { type: "langage", categorie: "doc", nom: "Markdown", niveau: 90, utilisation: "quotidienne" },

        { type: "java", categorie: "spec", nom: "Java SE", niveau: 90, utilisation: "quotidienne" },
        { type: "java", categorie: "spec", nom: "Java EE", niveau: 35, utilisation: "régulière" },
        { type: "java", categorie: "spring", nom: "Spring Framework", niveau: 90, utilisation: "quotidienne" },
        { type: "java", categorie: "spring", nom: "Spring Boot", niveau: 90, utilisation: "quotidienne" },
        { type: "java", categorie: "spring", nom: "Spring Security", niveau: 85, utilisation: "quotidienne" },
        { type: "java", categorie: "spring", nom: "Spring Data JPA", niveau: 95, utilisation: "quotidienne" },
        { type: "java", categorie: "spring", nom: "Spring MVC", niveau: 60, utilisation: "quotidienne" },
        { type: "java", categorie: "spring", nom: "Spring AMQP", niveau: 30, utilisation: "par le passé" },
        { type: "java", categorie: "spring", nom: "Spring Kafka", niveau: 30, utilisation: "quotidienne" },
        { type: "java", categorie: "spring", nom: "Spring Actuator", niveau: 30, utilisation: "ponctuelle" },
        { type: "java", categorie: "rcp", nom: "Swing", niveau: 30, utilisation: "par le passé" },
        { type: "java", categorie: "rcp", nom: "JWS", niveau: 30, utilisation: "par le passé" },
        { type: "java", categorie: "api", nom: "Servlet", niveau: 60, utilisation: "par le passé" },
        { type: "java", categorie: "api", nom: "JAX-WS", niveau: 50, utilisation: "par le passé" },
        { type: "java", categorie: "api", nom: "JAX-RS", niveau: 40, utilisation: "par le passé" },
        { type: "java", categorie: "api", nom: "JPA", niveau: 90, utilisation: "quotidienne" },
        { type: "java", categorie: "api", nom: "Java Scripting", niveau: 10, utilisation: "par le passé" },
        { type: "java", categorie: "web", nom: "JSP", niveau: 30, utilisation: "par le passé" },
        { type: "java", categorie: "web", nom: "Struts", niveau: 10, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "Feign", niveau: 70, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "Orika", niveau: 10, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "MapStruct", niveau: 65, utilisation: "quotidienne" },
        { type: "java", categorie: "lib", nom: "SLF4j", niveau: 80, utilisation: "quotidienne" },
        { type: "java", categorie: "lib", nom: "Logback", niveau: 75, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "JUnit", niveau: 85, utilisation: "quotidienne" },
        { type: "java", categorie: "lib", nom: "AssertJ", niveau: 90, utilisation: "quotidienne" },
        { type: "java", categorie: "lib", nom: "Mockito", niveau: 90, utilisation: "quotidienne" },
        { type: "java", categorie: "lib", nom: "EasyMock", niveau: 35, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "DbUnit", niveau: 25, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "Jasper", niveau: 10, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "Axis", niveau: 5, utilisation: "par le passé" },
        { type: "java", categorie: "lib", nom: "JUL", niveau: 10, utilisation: "par le passé" },
        { type: "java", categorie: "server", nom: "Glassfish", niveau: 20, utilisation: "par le passé" },
        { type: "java", categorie: "server", nom: "Tomcat", niveau: 20, utilisation: "par le passé" },

        { type: "web", categorie: "lib", nom: "AngularJS", niveau: 10, utilisation: "par le passé" },
        { type: "web", categorie: "lib", nom: "Angular", niveau: 30, utilisation: "ponctuelle" },
        { type: "web", categorie: "lib", nom: "React", niveau: 10, utilisation: "ponctuelle" },
        { type: "web", categorie: "lib", nom: "VueJs", niveau: 30, utilisation: "à titre personnel" },

        { type: "protocole", categorie: "web", nom: "SOAP", niveau: 60, utilisation: "par le passé" },
        { type: "protocole", categorie: "web", nom: "REST", niveau: 90, utilisation: "quotidienne" },
        { type: "protocole", categorie: "web", nom: "HTTP", niveau: 80, utilisation: "quotidienne" },
        { type: "protocole", categorie: "messaging", nom: "AMQP", niveau: 70, utilisation: "par le passé" },
        { type: "protocole", categorie: "auth", nom: "OpenID Connect", niveau: 60, utilisation: "régulière" },
        { type: "protocole", categorie: "auth", nom: "SAML", niveau: 35, utilisation: "régulière" },

        { type: "outil", categorie: "ide", nom: "IntelliJ", niveau: 80, utilisation: "quotidienne" },
        { type: "outil", categorie: "ide", nom: "NetBeans", niveau: 25, utilisation: "par le passé" },
        { type: "outil", categorie: "ide", nom: "Eclipse", niveau: 15, utilisation: "par le passé" },
        { type: "outil", categorie: "ide", nom: "VS Code", niveau: 40, utilisation: "quotidienne" },
        { type: "outil", categorie: "build", nom: "Gradle", niveau: 80, utilisation: "par le passé" },
        { type: "outil", categorie: "build", nom: "Maven", niveau: 40, utilisation: "quotidienne" },
        { type: "outil", categorie: "build", nom: "Ant", niveau: 25, utilisation: "par le passé" },
        { type: "outil", categorie: "build", nom: "Npm", niveau: 25, utilisation: "ponctuelle" },
        { type: "outil", categorie: "build", nom: "NodeJs", niveau: 25, utilisation: "ponctuelle" },
        { type: "outil", categorie: "scm", nom: "Git", niveau: 90, utilisation: "quotidienne" },
        { type: "outil", categorie: "scm", nom: "SVN", niveau: 20, utilisation: "par le passé" },
        { type: "outil", categorie: "container", nom: "Docker", niveau: 80, utilisation: "quotidienne" },
        { type: "outil", categorie: "container", nom: "Docker Compose", niveau: 80, utilisation: "ponctuelle" },
        { type: "outil", categorie: "rest", nom: "Open API", niveau: 35, utilisation: "quotidienne" },
        { type: "outil", categorie: "rest", nom: "RAML", niveau: 10, utilisation: "par le passé" },
        { type: "outil", categorie: "doc", nom: "PlantUML", niveau: 70, utilisation: "régulière" },
        { type: "outil", categorie: "doc", nom: "GraphViz", niveau: 20, utilisation: "ponctuelle" },
        { type: "outil", categorie: "doc", nom: "VuePress", niveau: 60, utilisation: "ponctuelle" },
        { type: "outil", categorie: "devops", nom: "Jenkins", niveau: 80, utilisation: "par le passé" },
        { type: "outil", categorie: "devops", nom: "Azure DevOps Pipelines", niveau: 15, utilisation: "par le passé" },
        { type: "outil", categorie: "devops", nom: "GitLab CI", niveau: 30, utilisation: "quotidienne" },
        { type: "outil", categorie: "devops", nom: "Ansible", niveau: 80, utilisation: "quotidienne" },
        { type: "outil", categorie: "devops", nom: "Helm", niveau: 20, utilisation: "quotidienne" },
        { type: "outil", categorie: "qualite", nom: "SonarQube", niveau: 80, utilisation: "quotidienne" },
        { type: "outil", categorie: "qualite", nom: "Findbugs", niveau: 15, utilisation: "par le passé" },
        { type: "outil", categorie: "qualite", nom: "PMD", niveau: 15, utilisation: "par le passé" },
        { type: "outil", categorie: "qualite", nom: "Checkstyle", niveau: 15, utilisation: "par le passé" },
        { type: "outil", categorie: "autre", nom: "WSL", niveau: 40, utilisation: "quotidienne" },

        { type: "bdd", categorie: "sql", nom: "PostgreSQL", niveau: 65, utilisation: "ponctuelle" },
        { type: "bdd", categorie: "sql", nom: "MySQL", niveau: 35, utilisation: "quotidienne" },
        { type: "bdd", categorie: "sql", nom: "MariaDb", niveau: 30, utilisation: "par le passé" },

        { type: "techno", categorie: "supervision", nom: "Elasticsearch", niveau: 20, utilisation: "par le passé" },
        { type: "techno", categorie: "supervision", nom: "Logstash", niveau: 35, utilisation: "par le passé" },
        { type: "techno", categorie: "supervision", nom: "Kibana", niveau: 40, utilisation: "par le passé" },
        { type: "techno", categorie: "supervision", nom: "Prometheus", niveau: 25, utilisation: "par le passé" },
        { type: "techno", categorie: "supervision", nom: "Grafana", niveau: 25, utilisation: "par le passé" },
        { type: "techno", categorie: "iam", nom: "Auth0", niveau: 80, utilisation: "quotidienne" },
        { type: "techno", categorie: "iam", nom: "Keycloak", niveau: 80, utilisation: "par le passé" },
        { type: "techno", categorie: "iam", nom: "CAS", niveau: 20, utilisation: "par le passé" },
        { type: "techno", categorie: "messaging", nom: "RabbitMq", niveau: 75, utilisation: "par le passé" },
        { type: "techno", categorie: "messaging", nom: "Kafka", niveau: 35, utilisation: "quotidienne" },
        { type: "techno", categorie: "infrastructure", nom: "GCP", niveau: 15, utilisation: "quotidienne" },
        { type: "techno", categorie: "infrastructure", nom: "Kubernetes", niveau: 15, utilisation: "quotidienne" },

        { type: "os", categorie: "os", nom: "Linux", niveau: 50, utilisation: "quotidienne" },
        { type: "os", categorie: "os", nom: "Windows", niveau: 30, utilisation: "quotidienne" },
    ],

    competencesProfessionnelles: [
        { nom: "Travail en équipe", niveau: 100 },
        { nom: "Travail en distanciel", niveau: 100 },
        { nom: "Autonomie", niveau: 95 },
        { nom: "Pédagogie", niveau: 80 },
        { nom: "Gestion de projet", niveau: 65 },
    ],

    entreprises: [
        {
            id: 1,
            nom: "Association Histoire d'Eau",
            site: "https://www.histoiredeauplongee.com",
            description: [ "Club de plongée à Erquy en Bretagne." ],
            contribution: null
        },
        {
            id: 2,
            nom: "Modularis",
            site: "http://www.modularis.fr",
            description: [ "Editeur de logiciels pour les collectivités." ],
            contribution: null
        },
        {
            id: 3,
            nom: "Artisan Clé France",
            site: null,
            description: [
                "Startup qui visait à créer un label de qualité pour les entreprises artisanales.",
                
                "Les artisans affiliés au réseau Artisan Clé France bénéficiaient d'un pack publicitaire (stickers, flyers, etc...) à leurs " +
                "couleurs et estampillé avec le logo du label, ainsi que d'un service de questionnaires de satisfaction client et d'une application " +
                "web de gestion des contacts. "
            ],
            contribution: [ 
                "Arrivé en tant que stagiaire, j'ai travaillé sur l'outil interne pour gérer les questionnaires de satisfaction client. " +
                "Puis j'ai été embauché pour développer l'application web \"2.0\" de gestion des contacts. "
            ]
        },
        {
            id: 4,
            nom: "Arche MC2",
            site: "https://arche-mc2.fr",
            description: [ "Editeur de logiciels et solutions dans le domaine du médico-social et du service à la personne." ],
            contribution: [
                "J'ai commencé en tant que développeur Java sur sur le module facturation de l'application nommée \"Solution Web\". " +
                "Après avoir développé la partie Portage de repas de ce module, j'ai eu l'opportunité de prendre le rôle de développeur leader et d'évoluer au fil des " +
                "années vers un rôle d'architecte logiciel. ",

                "Fort de cette expérience, j'ai travaillé sur de nouveaux projets sur lesquels j'ai pu participer à la " +
                "définition de l'architecture, à la conception du projet, à la mise en place du socle technique, de la CI/CD et aux développements. ",

                "Enfin j'ai pleinement endossé un rôle d'architecte sur le projet \"Arcad\" avec la mise en place d'une architecture de type microservice. " +
                "En tant que responsable technique du projet, j'ai accompagné activement chaque fonctionnalité dans les phases de conception et de développement. " +
                "En parallèle, j'ai progressivement mis en place tous les éléments nécessaires à la réussite du projet. " +
                "\"Arcad\" est depuis devenu une plateforme centrale dans l'écosystème des solutions de l'entreprise et de ses clients. "
            ]
        },
        {
            id: 5,
            nom: "Everysens",
            site: "https://www.everysens.com",
            description: [ "Editeur de solutions dans le domaine de la gestion de transports ferroviaires." ],
            contribution: null
        }
    ],

    experiencesProfessionnelles: [
        {
            id: 1,
            debut: new Date(2004, month(5), 2),
            fin: new Date(2004, month(6), 30),
            entreprise: "Association Histoire d'Eau",
            entrepriseId: 1,
            stage: true,
            projet: {
                nom: null,
                description: [ "Application de gestion des adhérents." ],
                contribution: null
            },
            roles: [
                { nom: "Développeur", type: null, objectifs: null }
            ],
            missions: null,
            environnementTechnique: [
                "Visual Basic"
            ]
        },
        {
            id: 2,
            debut: new Date(2005, month(2), 1),
            fin: new Date(2005, month(3), 31),
            entreprise: "Modularis",
            entrepriseId: 2,
            stage: true,
            projet: {
                nom: null,
                description: [ "Logiciel de visualisation des plans cadastraux informatisés." ],
                contribution: null
            },
            roles: [
                { nom: "Développeur", type: null, objectifs: null }
            ],
            missions: null,
            environnementTechnique: [
                "Visual Basic"
            ]
        },
        {
            id: 3,
            debut: new Date(2006, month(5), 2),
            fin: new Date(2006, month(6), 30),
            entreprise: "Artisan Clé France",
            entrepriseId: 3,
            stage: true,
            projet: {
                nom: "Fiche Qualité",
                description: [ "Outil interne pour gérer les questionnaires de satisfaction client." ],
                contribution: null
            },
            roles: [
                { nom: "Développeur", type: "fullstack", objectifs: null }
            ],
            missions: null,
            environnementTechnique: [
                "Java", "JSP", "Servlet", "Access"
            ]
        },
        {
            id: 4,
            debut: new Date(2006, month(7), 1),
            fin: new Date(2008, month(8), 1),
            entreprise: "Artisan Clé France",
            entrepriseId: 3,
            stage: false,
            projet: {
                nom: null,
                description: [ "Application web \"2.0\" pour la gestion des contacts des artisans affiliés au réseau." ],
                contribution: null
            },
            roles: [
                {
                    nom: "Développeur",
                    type: "fullstack",
                    objectifs: [
                        "Analyse, conception et développement",
                        "Rédaction du manuel utilisateur",
                        "Assistance téléphonique"
                    ]
                }
            ],
            missions: null,
            environnementTechnique: [
                "Java", "JSP", "HTML", "CSS", "MySQL", "Tomcat"
            ]
        },
        {
            id: 5,
            debut: new Date(2008, month(8), 8),
            fin: new Date(2011, month(12), 31),
            entreprise: "Arche MC2 (Apologic à cette période)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "Solution Web",
                description: [
                    "Application de gestion destinée aux entreprises et associations du domaine du service à la personne. "+
                    "Elle permet à ces acteurs de gérer leurs clients, leurs salariés, les plannings, d'effectuer la facturation, etc..."
                ],
                contribution: null
            },
            roles: [
                {
                    nom: "Développeur",
                    type: "Java",
                    objectifs: [
                        "Maintenance et évolution du module de facturation"
                    ]
                }
            ],
            missions: [
                { description: "Développement de la partie Portage de repas pour le module facturation" }
            ],
            environnementTechnique: [
                "Java", "JWS", "Swing", "SOAP (JAX-WS, Axis)", "JPA (EclipseLink)", "PostgreSQL", "Glassfish", "Ant", "SVN", "NetBeans"
            ]
        },
        {
            id: 6,
            debut: new Date(2009, month(5), 2),
            fin: new Date(2010, month(4), 30),
            entreprise: "Arche MC2 (Apologic à cette période)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "Domatel Extranet / Cadhoc",
                description: [
                    "Extranet pour la solution de télégestion (suivi en temps réel des intervenants d'aide à domicile) "+
                    "et site marchant du groupe Chèque Déjeuner (désormais Groupe Up) pour les chèques cadeaux."
                ],
                contribution: null
            },
            roles: [
                {
                    nom: "Développeur",
                    type: "fullstack",
                    objectifs: [
                        "Maintenance, évolution sur des pages web des sites",
                        "Edition d'états Jasper",
                        "Maintenance sur des systèmes d'import de données"
                    ]
                }
            ],
            missions: null,
            environnementTechnique: [
                "Java", "Spring Framework", "Struts", "Jasper", "JPA", "Maven", "Eclipse", "PostgreSQL"
            ]
        },
        {
            id: 7,
            debut: new Date(2012, month(1), 2),
            fin: new Date(2018, month(12), 31),
            entreprise: "Arche MC2 (Apologic à cette période)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "Solution Web",
                description: [
                    "Application de gestion destinée aux entreprises et associations du domaine du service à la personne. "+
                    "Elle permet à ces acteurs de gérer leurs clients, leurs salariés, les plannings, d'effectuer la facturation, etc...",

                    "Ecrite entièrement en Java (IHM Swing communiquant avec le backend via des Web-Services SOAP), cette application "+
                    "monolithique est assez imposante avec environ 1,5M de lignes de code. La partie serveur est multi-tenant, mais néanmoins "+
                    "déployée sur de multiples serveurs. Chaque instance pouvant être configurée de manière centralisée via une console d'administration "+
                    "maison appelée \"Base Technique\"."
                ],
                contribution: [
                    "Mes premiers travaux ont consistés à combler certaines lacunes. Il n'y avait, par exemple, pas de tests unitaires et rien n'était automatisé. "+
                    "J'ai par la suite continué à améliorer de nombreux aspects techniques. Avec sa taille importante et son mode de déploiement complexe, chaque "+
                    "chantier était un défi, souvent de longue haleine."
                ]
            },
            roles: [
                {
                    nom: "Architecte",
                    type: "logiciel",
                    objectifs: [
                        "Maintenance et évolution de l'architecture et de l'environnement technique"
                    ]
                },
                {
                    nom: "Développeur",
                    type: "leader",
                    objectifs: [
                        "Développements techniques",
                        "Définition et transmission des bonnes pratiques",
                        "Formations, animations techniques pour les développeurs"
                    ]
                }
            ],
            missions: [
                { description: "Automatisation des déploiements de l'application en recette/pré-production via Jenkins" },
                { description: "Mise en place d'outillage pour l'équipe de recette (via Jenkins)" },
                { description: "Mise en place des tests unitaires et d'intégrations" },
                { description: "Mise en place de Spring pour l'inversion de contrôle et l'injection de dépendances, ainsi que la gestion des transactions" },
                { description: "Finalisation d'un système de synchronisation entre les différentes instances de l'application" },
                { description: "Chantiers de montées en versions", liste: [
                    "JPA : 800 impacts dans le code à traiter",
                    "JDK + serveur d'applications : déploiement progressif nécessitant une rétrocompatibilité"
                ] },
                { description: "Analyse et définition de solutions par rapport à des problématiques de performances ciblées" },
                { description: "Conception d'un système pour outrepasser JWS vis-à-vis de problèmes de performances" },
                { description: "Initialisation d'une \"plateforme\" de supervision fonctionnelle centralisée avec ELK" },
                { description: "Mise en place d'APIs REST et sécurisation avec Spring Security" },
                { description: "Migration des sources sur Git" },
            ],
            environnementTechnique: [
                "Java (7 puis 8)", "JWS", "Swing", "SOAP (JAX-WS, Axis)", "Spring Framework", "JPA (EclipseLink)", "PostgreSQL", 
                "Glassfish", "Tomcat", "Ant", "Gradle", "SVN", "Git", "NetBeans", "IntelliJ", "JUnit", "EasyMock", "DbUnit",
                "JUL", "Jenkins", "Findbugs", "PMD", "Checkstyle", "SonarQube", "ELK", "Spring AMQP (RabbitMq)"
            ]
        },
        {
            id: 8,
            debut: new Date(2014, month(9), 1),
            fin: new Date(2015, month(6), 30),
            entreprise: "Arche MC2 (Apologic à cette période)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "Espace Déclaratif",
                description: [
                    "Portail web destiné au calcul et à la gestion des DSN (Déclarations Sociales Nominatives)."
                ],
                contribution: [
                    "Je participe avec un autre architecte à l'élaboration de l'architecture et de la conception du projet. "+
                    "Nous faisons le choix d'une application web d'un côté, et d'un autre côté, de la mise en oeuvre de différents batchs "+
                    "afin de déporter les traitements lourds de calcul des déclarations dans d'autres processus. Ces choix permettent de "+
                    "ne pas impacter l'application web et de pouvoir mettre à l'échelle ces traitements."
                ]
            },
            roles: [
                {
                    nom: "Architecte",
                    type: "logiciel",
                    objectifs: [
                        "Conception de l'architecture et de l'application"
                    ]
                },
                {
                    nom: "Développeur",
                    type: "fullstack",
                    objectifs: [
                        "Développements fonctionnels"
                    ]
                }
            ],
            missions: [
                { description: "Conception de l'architecture : application web et batchs de traitements asynchrones" },
                { description: "Mise en place du système de build (Gradle)" },
                { description: "Mise en place en collaboration avec l'équipe système des environnements de recette/pré-production et production" },
                { description: "Automatisation des déploiements en recette/pré-production" },
            ],
            environnementTechnique: [
                "Java 8", "Spring Framework", "Spring Data JPA", "Spring MVC", "Spring Security", "JPA",
                "PostgreSQL", "HTML", "CSS", "Javascript", "AngularJS", "Gradle", "SVN", "Eclipse",
                "Jenkins", "SonarQube", "Junit", "Mockito", "AssertJ", "Logback", "Logstash", "ElasticSearch", "Kibana"
            ]
        },
        {
            id: 9,
            debut: new Date(2016, month(10), 1),
            fin: new Date(2017, month(6), 30),
            entreprise: "Arche MC2 (Apologic à cette période)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "Base Technique",
                description: [
                    "Application interne permettant d'administrer les instances de la Solution Web, leurs paramétrages, leurs mises à jour, "+
                    "la diffusion d'actualité, la supervision du système de synchronisation, etc... Le projet est une réécriture de la précédente version, "+
                    "permettant également de mutualiser des développements vis-à-vis des besoins de la gamme WinDev d'Apologic."
                ],
                contribution: null
            },
            roles: [
                {
                    nom: "Architecte",
                    type: "logiciel",
                    objectifs: [
                        "Conception et mise en place de l'architecture et de l'application"
                    ]
                },
                {
                    nom: "Développeur",
                    type: "fullstack",
                    objectifs: [
                        "Développements fonctionnels"
                    ]
                },
                {
                    nom: "Maître de stage",
                    type: null,
                    objectifs: [
                        "Suivi et accompagnement d'un stagiaire présent au démarrage du projet"
                    ]
                },
                {
                    nom: "Chef de projet",
                    type: "MOE",
                    objectifs: [
                        "Suivi et organisation du projet (accompagné par mon responsable)"
                    ]
                }
            ],
            missions: [
                { description: "Mise en place de l'architecture dans l'esprit microservice" },
                { description: "Mise en place d'un \"archétype\" de module" },
                { description: "Sécurisation via le protocole (et serveur) CAS et Spring Security" },
                { description: "Migration des données depuis la version précédente" },
                { description: "Mise en place, en collaboration avec l'équipe système, des environnements de recette/pré-production et production" },
                { description: "Automatisation des déploiements en recette/pré-production" },
            ],
            environnementTechnique: [
                "Java 8", "Spring Framework", "Spring Boot", "Spring Data JPA", "Spring MVC", "Spring Security",
                "JPA", "PostgreSQL", "CAS", "HTML", "CSS", "Javascript", "AngularJS", "Gradle", "Git", "GitLab", "Eclipse", 
                "Jenkins", "SonarQube", "Junit", "Mockito", "AssertJ"
            ]
        },
        {
            id: 10,
            debut: new Date(2017, month(10), 1),
            fin: new Date(2019, month(10), 31),
            entreprise: "Arche MC2 (Cityzen à cette période)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "OneHome",
                description: [
                    "La société a fusionné avec 4 autres filiales du Groupe Up, donnant naissance à Cityzen. "+
                    "Une équipe d'architectes transverses a été créée afin de mettre en place une architecture "+
                    "permettant d'arriver à une plateforme de services. Le principal objectif est de permettre "+
                    "aux différentes applications métier des ex-filiales de pouvoir mettre en commun et synchroniser leurs données. "+
                    "Cette plateforme doit aussi être le socle pour de nouveaux services et permettre de mutualiser certains développements.",

                    "Le projet est basé sur une application métier existante, de type multi-instance, écrite en .Net. Deux autres applications métier "+
                    "viennent s'y brancher au travers d'APIs REST et d'un bus de données."
                ],
                contribution: [
                    "L'équipe part rapidement sur la mise en oeuvre d'un système de messaging (RabbitMq) pour simplifier les échanges entres "+
                    "les différentes applications. En parallèle un serveur d'authentification Open Id Connect (Keycloak) est mis en place. "+
                    "Il s'agira de mes 2 principaux sujets sur ce projet."
                ]
            },
            roles: [
                {
                    nom: "Architecte",
                    type: "de solution",
                    objectifs: [
                        "Définition et mise en place de l'architecture de la plateforme",
                        "Accompagnement des équipes de développement (intégration des applications métier dans la plateforme et amélioration de la productivité (CI, qualité, ...))",
                    ]
                }
            ],
            missions: [
                { description: "Entre octobre 2017 et avril 2018, POC du projet OneHome", liste: [
                    "Mise en place d'un broker AMQP (RabbitMq) ; développements associés dans l'une des applications métier concernée par le POC (Solution Web)",
                    "Participation aux travaux sur l'authentification avec Open Id Connect",
                    "Participation aux travaux sur l'ouverture des applications de type client lourd depuis une application web"
                ] },
                { description: "Mise en place d'un script Ansible pour automatiser la configuration de RabbitMq" },
                { description: "Cartographie technique (technos, architecture, CI, ...) des différentes applications des ex-filiales" },
                { description: "Elaboration d'un dossier d'architecture de la future plateforme" },
                { description: "Comparatif entre Azure DevOps et GitLab" },
                { description: "Mise en place d'un serveur d'authentification Open Id Connect (Keycloak)", liste: [
                    "Tests comparatifs avec Microsoft AD B2C",
                    "Conception et rédaction des spécifications techniques",
                    "Développement d'une application en façade du serveur"
                ] },
                { description: "Travaux autour du bus messages", liste: [
                    "Comparatif avec Kafka",
                    "Conception d'une nouvelle topologie AMQP",
                    "Rédaction des spécifications techniques"
                ] },
                { description: "Conception de l'architecture pour le 1er projet concret de la plateforme de service : Portail Salarié" }
            ],
            environnementTechnique: [
                "Java 11", "Spring Boot", "Spring Security", ".Net", "C#", "React", "Keycloak", "RabbitMq", "Git", "GitLab", "Docker", "Ansible", "Jenkins", "SonarQube"
            ]
        },
        {
            id: 11,
            debut: new Date(2019, month(11), 1),
            fin: new Date(2022, month(7), 22),
            entreprise: "Arche MC2 (Cityzen jusqu'à mars 2022)",
            entrepriseId: 4,
            stage: false,
            projet: {
                nom: "Arcad",
                description: [
                    "Ce projet est un deuxième départ suite à l'arrêt du projet OneHome qui n'a pas eut le succès escompté. Une équipe réduite est "+
                    "missionnée pour repartir de zéro, sur de meilleures bases. Les objectifs restent similaires : pouvoir faire communiquer "+
                    "les différentes solutions métier, mutualiser des développements et offrir de nouveaux services au travers une plateforme moderne."
                ],
                contribution: [
                    "En tant que responsable technique du projet, j'ai fais le choix d'une architecture de type microservice multi-tenant pour de multiples raisons. "+
                    "Certains fondements du projet OneHome sont repris comme l'usage de RabbitMq et de Keycloak. ",
                    
                    "J'accompagne activement chaque fonctionnalité dans les phases de conception et de développement. En parallèle, je mets progressivement en place tous les "+
                    "éléments nécessaires à la réussite du projet.",

                    "Arcad a démarré en production par une phase pilote entre décembre 2020 et avril 2021. Depuis nos clients sont régulièrement branchés sur ce nouveau "+
                    "système et bénéficient ainsi de la synchronisation de leurs données entre leurs différentes solutions métier et des premiers services qui ont été développés."
                ]
            },
            roles: [
                {
                    nom: "Architecte",
                    type: "de solution",
                    objectifs: [
                        "Définition et mise en place de l'architecture globale, type microservice",
                        "Accompagnement des projets et des équipes de développement (architecture, conception, RDC, suivi, etc...)"
                    ]
                },
                {
                    nom: "Architecte",
                    type: "logiciel",
                    objectifs: [
                        "Mise en place et évolution du socle technique Java"
                    ]
                },
                {
                    nom: "Développeur",
                    type: "leader",
                    objectifs: [
                        "Accompagnement des projets et des équipes de développement (architecture, conception, RDC, suivi, etc...)",
                        "Moments d'échange et de partage (bonnes pratiques, etc...)"
                    ]
                },
                {
                    nom: "Développeur",
                    type: "backend",
                    objectifs: [
                        "Développements techniques",
                        "Développements fonctionnels"
                    ]
                },
                {
                    nom: "DevOps",
                    type: null,
                    objectifs: [
                        "Gestion des environnements pré-production, CI, livraison",
                        "Déploiement des nouveaux services"
                    ]
                }
            ],
            missions: [
                { description: "Définition de l'architecture globale, type microservice", liste: [
                    "Rédaction d'un dossier d'architecture et de conception",
                    "Reprise des spécifications techniques Keycloak et RabbitMq"
                ] },
                { description: "Mise en place du socle technique des microservices Java", liste: [
                    "Mise en place d'un service \"template\" opérationnel",
                    "Mise en place de librairies mutualisables entre services Java"
                ] },
                { description: "Mise en place des logs et métriques au niveau des services .Net" },
                { description: "Mise en place d'outils pour le développement", liste: [
                    "Dockerisation des briques d'infrastructure",
                    "Dockerisation des microservices et applications front"
                ] },
                { description: "Mise en place des environnements pré-production et de l'intégration continue", liste: [
                    "Pipeline CI : automatisation des builds, passage des TU, outils de qualité",
                    "Configuration de l'infrastructure Keycloak, RabbitMq",
                    "Déploiement automatisé des configurations pour ELK, Prometheus/Grafana",
                    "Déploiement automatisé des microservices"
                ] },
                { description: "Configuration de l'infrastructure Keycloak, RabbitMq en préproduction et production" },
                { description: "Développement du service de gestion des utilisateurs" },
                { description: "Développement du service de gestion des tenants et d'automatisation de la configuration de l'infrastructure Keycloak et RabbitMq" },
                { description: "Quelques maintenances techniques dans les services .Net" },
                { description: "Mise en place de la déléguation de l'authentification à Pro Santé Connect" },
                { description: "Accompagnement, conception et suivi de différents projets fonctionnels", liste: [
                    "Annuaire santé",
                    "Couplage Téléphonique",
                    "Interfaçage avec le partenaire Dome",
                    "Système de notifications (mobile et web)",
                    "..."
                ] },
            ],
            environnementTechnique: [
                "Java", "Spring Boot", "Spring MVC", "Spring Security", "Spring Data JPA", "Spring AMQP", "Spring Actuator", 
                ".Net Core", "C#", "Swagger (Open API 3)", "MariaDb (PosgreSQL précédemment)", "Keycloak", "RabbitMq", 
                "Elasticsearch", "Logstash", "Kibana", "Prometheus", "Grafana", "Docker", "Typescript", "React", "React Native",
                "Git", "Azure DevOps", "Jenkins", "SonarQube", "Nexus"
            ]
        },
        {
            id: 12,
            debut: new Date(2022, month(7), 26),
            fin: null,
            entreprise: "Everysens",
            entrepriseId: 5,
            stage: false,
            projet: {
                nom: "TVMS",
                description: [
                    "TVMS pour Transport and Visibility Managment System, une plateforme de gestion et du suivi de transport ferroviaire de marchandises."
                ],
                contribution: null
            },
            roles: [
                {
                    nom: "Développeur",
                    type: "backend",
                    objectifs: [
                        "Développements fonctionnels en backend (Java)"
                    ]
                },
                {
                    nom: "Développeur",
                    type: "frontend",
                    objectifs: [
                        "Quelques développements fonctionnels côté frontend (Angular)"
                    ]
                },
                {
                    nom: "DevOps",
                    type: null,
                    objectifs: [
                        "Relais entre l'équipe infrastructure et l'équipe de développement",
                        "Maintenance, évolution dans les pipelines GitLab CI"
                    ]
                }
            ],
            missions: [
                { description: "Mise en place de la démarche FinOps" },
                { description: "Optimisation du moteur de planification des transports" },
                { description: "Développement d'un nouveau service Java pour gérer la configuration de la plateforme" },
                { description: "Test de la solution permit.io pour le compte de la direction technique" },
                { description: "Migration vers Java 21 et Spring Boot 3" },
                { description: "Responsable des aspects IAM et intégrations SSO" }
            ],
            environnementTechnique: [
                "Java", "Spring Boot", "Spring Security", "Spring Data JPA", "Spring Kafka", "Spring Actuator", 
                "Swagger (Open API 3)", "PosgreSQL", "Auth0", "Kafka", "Docker", "Git", "GitLab", "SonarQube", "JFrog",
                "GCP", "Kubernetes", "Helm", "Angular", "Typescript"
            ]
        }
    ],

    formationsInitiales: [
        {
            anneeDebut: 2001,
            anneeFin: 2003,
            type: "BAC",
            niveau: 0,
            libelle: "STAE",
            libelleLong: "Science et Technique de l'Agronomie et de l'Environnement",
            mention: "assez bien",
            etablissement: {
                nom: "Lycée agricole Auguste Loutreuille",
                ville: "Sées",
                numDepartement: "61"
            }
        },
        {
            anneeDebut: 2003,
            anneeFin: 2005,
            type: "BTS",
            niveau: 2,
            libelle: "Informatique de gestion",
            libelleLong: "Informatique de gestion, option développeur d'application",
            mention: null,
            etablissement: {
                nom: "Lycée Rablais",
                ville: "Saint-Brieuc",
                numDepartement: "22"
            }
        },
        {
            anneeDebut: 2005,
            anneeFin: 2006,
            type: "Licence",
            niveau: 3,
            libelle: "Informatique",
            libelleLong: "Informatique",
            mention: "assez bien",
            etablissement: {
                nom: "IUP Ingérieurie informatique",
                ville: "Brest",
                numDepartement: "29"
            }
        }
    ],

    formationsContinues: [
        {
            annee: 2013,
            nbJours: 3,
            libelle: "JPA avancé"
        },
        {
            annee: 2013,
            nbJours: 3,
            libelle: "Glassfish 2 administration avancé"
        },
        {
            annee: 2013,
            nbJours: 2,
            libelle: "HTML5 et CSS3 pour les designers et intégrateurs web"
        },
        {
            annee: 2019,
            nbJours: 2,
            libelle: "Découverte Microsoft Azure"
        },
        {
            annee: 2020,
            nbJours: 3,
            libelle: "RabbitMq"
        },
        {
            annee: 2021,
            nbJours: 3,
            libelle: "Kubernetes Application Developer"
        },
        {
            annee: 2024,
            nbJours: 4,
            libelle: "Cours d'anglais"
        }
    ]
}

const store = createStore({
    state() {
        return {
            themeName: 'light',
            theme: null,

            introPlayed: false,

            cv: cv
        }
    },
    
    getters: {
        anneesExperience(state) {
            return new Number((new Date().getTime() - state.cv.debutCarriere.getTime()) / 31536000000).toFixed(0)
        },

        nbTechnosMinimumMaitrise(state) {
            return state.cv.competencesTechniques.filter(ct => ct.niveau >= 30).length
        },

        nbProjetsMissions(state) {
            let nbProjets = state.cv.experiencesProfessionnelles.length
            let nbMissions = state.cv.experiencesProfessionnelles
                .filter(exp => exp.missions !== null)
                .flatMap(exp => exp.missions)
                .flatMap(m => {
                    let missions = [m.description]
                    if(m.liste) {
                        m.liste.forEach(e => missions.push(e))
                    }
                    return missions
                })
                .length

            return nbProjets + nbMissions
        },

        nbProjetsRoleArchitecte(state) {
            return state.cv.experiencesProfessionnelles
                .filter(exp => exp.roles.some(r => r.nom === 'Architecte'))
                .length
        },

        findCompetencesTechniquesBy: (state) => (types, niveau, utilisations) => {
            return state.cv.competencesTechniques.filter(ct => 
                types.includes(ct.type) &&
                ct.niveau >= niveau &&
                utilisations.includes(ct.utilisation)
            )
        }
    },
    
    mutations: {
        changeTheme(state, themeName) {
            state.themeName = themeName
            state.theme = themeName === 'dark' ? darkTheme : null
        },

        introDone(state) {
            state.introPlayed = true
        }
    }
})

export default store