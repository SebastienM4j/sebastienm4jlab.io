import { createApp } from 'vue'
import AppCvPapierSummary from './AppCvPapierSummary.vue'
import store from './store'
import naive, { NThemeEditor } from 'naive-ui'

createApp(AppCvPapierSummary)
    .use(store)
    .use(naive)
    //.component('NThemeEditor', NThemeEditor)
    .mount('#app')
