FROM node:16.13.0 AS builder

WORKDIR /home/node

COPY ./public ./public
COPY ./src ./src
COPY ./babel.config.js ./babel.config.js
COPY ./package*.json ./

RUN npm install && \
    npm run build && \
    cp dist/index.html dist/404.html


FROM nginx:1.20.2

COPY docker-nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=builder /home/node/dist /usr/share/nginx/html